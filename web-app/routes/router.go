package routes

import "net/http"
import "../views"

func Router() {
	http.HandleFunc("/", views.HandlerIndex)
	http.HandleFunc("/index", views.HandlerIndex)
	http.HandleFunc("/hello", views.HandlerHello)
}
