package main

import "fmt"
import "net/http"
import "./routes"

func main() {
	routes.Router()

	var address = "localhost:9000"
	fmt.Printf("server started at %s\n", address)
	err := http.ListenAndServe(address, nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}
