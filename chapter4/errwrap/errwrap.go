package errwrap

import (
	"fmt"

	"github.com/pkg/errors"
)

// WrappedError demonstrates error wrapping and
// annotating an error
func WrappedError(e error) error {
	return errors.Wrap(e, "An error occurred in WrappedError")
}

// ErrorTyped is an error we can check against
type ErrorTyped struct {
	error
	//this section means that it just required
	//an attribut that is implement error interface
}

type CustomError struct {
	Result string
}

func (c CustomError) Error() string {
	return fmt.Sprintf("there was an error; %s was the result", c.Result)
}

// Wrap shows what happens when we wrap an error
func Wrap() {
	e := errors.New("standard error")

	fmt.Println("Regular Error - ", WrappedError(e))

	//fmt.Println("Typed Error - ",
	//	WrappedError(ErrorTyped{errors.New("typed error")}))

	fmt.Println("Typed Error - ",
		WrappedError(ErrorTyped{CustomError{Result: "HEHE"}}))

	fmt.Println("Nil -", WrappedError(nil))
}
